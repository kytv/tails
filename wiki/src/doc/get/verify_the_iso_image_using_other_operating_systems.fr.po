# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2015-03-23 02:53+0100\n"
"PO-Revision-Date: 2014-03-17 12:12+0100\n"
"Last-Translator: amnesia <amnesia@boum.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.4\n"

#. type: Content of: outside any tag (error?)
#, fuzzy
#| msgid ""
#| "[[!meta title=\"Verify the ISO image using other operating systems\"]] [[!"
#| "toc]]"
msgid ""
"[[!meta title=\"Verify the ISO image using other operating systems\"]] [[!"
"inline pages=\"doc/get/signing_key_transition.inline\" raw=\"yes\"]] [[!toc]]"
msgstr ""
"[[!meta title=\"Verifier l'image ISO en utilisant d'autres sytèmes "
"d'exploitation\"]] [[!toc]]"

#. type: Content of: <p>
msgid ""
"GnuPG, a common free software implementation of OpenPGP has versions and "
"graphical frontends for both Windows and Mac OS X. This also make it "
"possible to check the cryptographic signature with those operating systems:"
msgstr ""
"GnuPG est une implémentation d'OpenPGP comportant une interface graphique "
"pour Windows et Mac OS X, qui rend possible la vérification de signatures "
"cryptographiques avec ces systèmes d'exploitation :"

#. type: Content of: <ul><li>
msgid "[[Gpg4win|http://www.gpg4win.org/]], for Windows"
msgstr "[[Gpg4win|http://www.gpg4win.org/]], pour Windows"

#. type: Content of: <ul><li>
msgid "[[GPGTools|http://www.gpgtools.org/]], for Mac OS X"
msgstr "[[GPGTools|http://www.gpgtools.org/]], pour Mac OS X"

#. type: Content of: <p>
msgid ""
"You will find on either of those websites detailed documentation on how to "
"install and use them."
msgstr ""
"Vous trouverez sur ces sites internet une documentation détaillée sur "
"l'installation et l'usage de ces logiciels."

#. type: Content of: <h3>
msgid "For Windows using Gpg4win"
msgstr "Pour Windows et Gpg4win"

#. type: Content of: <p>
msgid "After installing Gpg4win, download Tails signing key:"
msgstr ""
"Après l'installation de Gpg4win, téléchargez la clef de signature Tails :"

#. type: Content of: outside any tag (error?)
msgid "[[!inline pages=\"lib/download_tails_signing_key\" raw=\"yes\"]]"
msgstr ""

#. type: Content of: <p>
msgid ""
"[[Consult the Gpg4win documentation to import it|http://www.gpg4win.org/doc/"
"en/gpg4win-compendium_15.html]]"
msgstr ""
"[[Consultez la documentation de Gpg4win pour l'importer|http://www.gpg4win."
"org/doc/en/gpg4win-compendium_15.html]]"

#. type: Content of: <p>
msgid ""
"Then, download the cryptographic signature corresponding to the ISO image "
"you want to verify:"
msgstr ""
"Maintenant téléchargez la signature numérique correspondant au fichier ISO "
"que vous souhaitez vérifier :"

#. type: Content of: outside any tag (error?)
msgid "[[!inline pages=\"lib/download_stable_i386_iso_sig\" raw=\"yes\"]]"
msgstr ""

#. type: Content of: <p>
msgid ""
"[[Consult the Gpg4win documentation to check the signature|http://www."
"gpg4win.org/doc/en/gpg4win-compendium_24.html#id4]]"
msgstr ""
"[[Consultez la documentation de Gpg4win pour vérifier la signature|http://"
"www.gpg4win.org/doc/en/gpg4win-compendium_24.html#id4]]"

#. type: Content of: <p>
msgid "If you see the following warning:"
msgstr "Si vous voyez l'avertissement suivant :"

#. type: Content of: outside any tag (error?)
msgid "<a id=\"warning\"></a>"
msgstr ""

#. type: Content of: <pre>
#, no-wrap
msgid ""
"Not enough information to check the signature validity.\n"
"Signed on ... by tails@boum.org (Key ID: 0x58ACD84F\n"
"The validity of the signature cannot be verified.\n"
msgstr ""

#. type: Content of: <p>
msgid ""
"Then the ISO image is still correct, and valid according to the Tails "
"signing key that you downloaded. This warning is related to the trust that "
"you put in the Tails signing key. See, [[Trusting Tails signing key|doc/get/"
"trusting_tails_signing_key]]. To remove this warning you would have to "
"personally <span class=\"definition\">[[!wikipedia Keysigning desc=\"sign\"]]"
"</span> the Tails signing key with your own key."
msgstr ""
"Alors l'image ISO est quand même bonne, et valide d'après la clé de "
"signature de Tails que vous avez téléchargé. Cet avertissement est lié à la "
"confiance que vous avez placé dans la clé de signature de Tails. Voir "
"[[Faire confiance à la clé de signature de Tails|doc/get/"
"trusting_tails_signing_key]]. Pour supprimer cet avertissement, vous devrez "
"personnellement <span class=\"definition\">[[!wikipedia Keysigning desc="
"\"signer\"]](en anglais)</span> la clé de signature de Tails avec votre clé."

#. type: Content of: <h3>
msgid "For Mac OS X using GPGTools"
msgstr "Pour Mac OS X et GPGTools"

#. type: Content of: <p>
msgid ""
"After installing GPGTools, you should be able to follow the instruction for "
"[[Linux with the command line|verify_the_iso_image_using_the_command_line]]. "
"To open the command line, navigate to your Applications folder, open "
"Utilities, and double click on Terminal."
msgstr ""
"Après l'installation de GPGTools, vous devriez être en mesure de suivre les "
"instructions <a href=\"#verify-the-iso-with-terminal\">\"Linux et lignes de "
"commandes\"</a>. Pour ouvrir un terminal, ouvrez votre dossier Applications, "
"puis Utilities/Utilitaires et double-cliquez sur Terminal."

#~ msgid "Using the cryptographic signature"
#~ msgstr "Utiliser la signature numérique"

#~ msgid "Using Firefox"
#~ msgstr "Avec Firefox"

#~ msgid ""
#~ "Instead of a cryptographic signature, this technique uses a cryptographic "
#~ "hash. We propose it because it's especially easy for Windows users."
#~ msgstr ""
#~ "À la place d'une signature cryptographique, cette technique utilise un "
#~ "hash cryptographique. Nous la proposons car elle est particulièrement "
#~ "simple pour les utilisateurs de Windows."

#~ msgid ""
#~ "Install the <span class=\"application\"><a href=\"https://addons.mozilla."
#~ "org/en-US/firefox/addon/md5-reborned-hasher/\">MD5 Reborned Hasher</a></"
#~ "span> extension for Firefox."
#~ msgstr ""
#~ "Installer l'extension <span class=\"application\"><a href=\"https://"
#~ "addons.mozilla.org/fr/firefox/addon/md5-reborned-hasher/\">MD5 Reborned "
#~ "Hasher</a></span> pour Firefox."

#~ msgid "Restart Firefox."
#~ msgstr "Redémarrer Firefox."

#~ msgid ""
#~ "Open the <span class=\"guilabel\">Downloads</span> window from the menu "
#~ "<span class=\"menuchoice\"> <span class=\"guimenu\">Tools</span>&nbsp;▸ "
#~ "<span class=\"guimenuitem\">Downloads</span></span>.  <span class="
#~ "\"application\">MD5 Reborned Hasher</span> only operates from the files "
#~ "that are appearing in the <span class=\"guilabel\">Downloads</span> "
#~ "window of Firefox."
#~ msgstr ""
#~ "Ouvrir la fenêtre de <span class=\"guilabel\">Téléchargements</span> "
#~ "depuis le menu <span class=\"menuchoice\"> <span class=\"guimenu"
#~ "\">Outils</span>&nbsp;▸ <span class=\"guimenuitem\">Téléchargements</"
#~ "span></span>.  <span class=\"application\">MD5 Reborned Hasher</span> "
#~ "fonctionne uniquement sur les fichiers qui apparaissent dans la fenêtre "
#~ "<span class=\"guilabel\">Téléchargements</span> de Firefox."

#~ msgid ""
#~ "<strong>If you are using Firefox 20 to 25</strong>, <span class="
#~ "\"application\">MD5 Reborned Hasher</span> is incompatible with the new "
#~ "<span class=\"guilabel\">Downloads</span> window. To go back to a "
#~ "compatible layout of the <span class=\"guilabel\">Downloads</span> "
#~ "window, do the following:"
#~ msgstr ""
#~ "<strong>Si vous utilisez Firefox version 20 à 25</strong>, <span class="
#~ "\"application\">MD5 Reborned Hasher</span> est incompatible avec la "
#~ "nouvelle fenêtre de <span class=\"guilabel\">Téléchargements</span>. Pour "
#~ "revenir a une fenêtre de <span class=\"guilabel\">Téléchargements</span> "
#~ "compatible, faire ceci :"

#~ msgid "Open a new tab in <span class=\"application\">Firefox</span>."
#~ msgstr ""
#~ "Ouvrir un nouvel onglet dans <span class=\"application\">Firefox</span>."

#~ msgid ""
#~ "Type <code>about:config</code> in the URL bar, then press <span class="
#~ "\"keycap\">Enter</span>."
#~ msgstr ""
#~ "Écrire <code>about:config</code> dans la barre d'URL, puis appuyer sur "
#~ "<span class=\"keycap\">Entrée</span>."

#~ msgid ""
#~ "Paste the following preference name into the <span class=\"guilabel"
#~ "\">search</span> field: <code>browser.download.useToolkitUI</code>."
#~ msgstr ""
#~ "Coller le nom de préférence suivant dans le champ de <span class="
#~ "\"guilabel\">recherche</span> :<code>browser.download.useToolkitUI</code>."

#~ msgid ""
#~ "Change the value of this preference to <span class=\"guilabel\">true</"
#~ "span>, by doing right-click on the preference and choosing <span class="
#~ "\"guilabel\">Toggle</span>."
#~ msgstr ""
#~ "Changer la valeur de cette préférence à <span class=\"guilabel\">true</"
#~ "span>, en faisant clic-droit sur la préférence et en choisissant <span "
#~ "class=\"guilabel\">Inverser</span>."

#~ msgid "Restart <span class=\"application\">Firefox</span>."
#~ msgstr "Redémarrer <span class=\"application\">Firefox</span>."

#~ msgid ""
#~ "<strong>If you are using Firefox 26 or later</strong>, this method is not "
#~ "working anymore. It is currently impossible to use Firefox 26 or later to "
#~ "verify an ISO image."
#~ msgstr ""
#~ "<strong>Si vous utilisez Firefox version 26 ou supérieure</strong>, cette "
#~ "méthode ne fonctionne plus. Il n'est actuellement pas possible d'utiliser "
#~ "ces versions de Firefox pour vérifier une image ISO."

#~ msgid "If the ISO image does not appear in the list of recent downloads:"
#~ msgstr ""
#~ "Si l'image ISO n'apparaît pas dans la liste des téléchargements récents : "

#~ msgid ""
#~ "Choose the menu <span class=\"menuchoice\"> <span class=\"guimenu\">File</"
#~ "span>&nbsp;▸ <span class=\"guimenuitem\">Open File…</span></span>."
#~ msgstr ""
#~ "Aller dans le menu <span class=\"menuchoice\"> <span class=\"guimenu"
#~ "\">Fichier</span>&nbsp;▸ <span class=\"guimenuitem\">Ouvrir un fichier…</"
#~ "span></span>."

#~ msgid ""
#~ "Select the ISO image that you want to check. Choose to save it with the "
#~ "same name. Answer <span class=\"guilabel\">Yes</span> if Firefox asks you "
#~ "whether you want to replace it."
#~ msgstr ""
#~ "Sélectionner l'image ISO que vous voulez vérifier. Choisir de sauvegarder "
#~ "l'image en gardant le même nom. Répondre <span class=\"guilabel\">Oui</"
#~ "span> si Firefox vous demande si vous voulez la remplacer."

#~ msgid ""
#~ "This starts a local copy of the ISO image and adds it to the <span class="
#~ "\"guilabel\">Downloads</span> window."
#~ msgstr ""
#~ "Ceci lance une copie locale de l'image ISO et l'ajoute à la fenêtre des "
#~ "<span class=\"guilabel\">Téléchargementss</span>."

#~ msgid ""
#~ "Click on the <span class=\"guilabel\">Check Digest…</span> link on the "
#~ "line of the <span class=\"guilabel\">Downloads</span> window "
#~ "corresponding to the ISO image. If no <span class=\"guilabel\">Check "
#~ "Digest…</span> link appear, then <span class=\"application\">MD5 Reborned "
#~ "Hasher</span> is not installed correctly."
#~ msgstr ""
#~ "Cliquer sur le lien <span class=\"guilabel\">Check Digest…</span> sur la "
#~ "ligne de la fenêtre des <span class=\"guilabel\">Téléchargements</span> "
#~ "correspondant à l'image ISO. Si le lien <span class=\"guilabel\">Check "
#~ "Digest…</span> n'apparaît pas, cela signifie que <span class=\"application"
#~ "\">MD5 Reborned Hasher</span> n'est pas correctement installé."

#~ msgid ""
#~ "In the <span class=\"guilabel\">Check File</span> window, choose a <span "
#~ "class=\"guilabel\">SHA256</span> hash type."
#~ msgstr ""
#~ "Dans la fenêtre <span class=\"guilabel\">Check File</span>, choisir <span "
#~ "class=\"guilabel\">SHA256</span> comme type de hashage."

#~ msgid "Click on <span class=\"guilabel\">Generate Digest</span>."
#~ msgstr "Cliquer sur <span class=\"guilabel\">Generate Digest</span>."

#~ msgid ""
#~ "Copy and paste the following hash for version [[!inline pages=\"inc/"
#~ "stable_i386_version\" raw=\"yes\"]] in the <span class=\"guilabel\">Enter "
#~ "checksum</span> text box."
#~ msgstr ""
#~ "Copier et coller le hash suivant correspondant à la version [[!inline "
#~ "pages=\"inc/stable_i386_version\" raw=\"yes\"]] dans la zone de texte "
#~ "<span class=\"guilabel\">Enter checksum</span>."

#~ msgid ""
#~ "When the hash is done generating, a result appears at the bottom of the "
#~ "window saying:"
#~ msgstr ""
#~ "Lorsque que la génération hash est terminée, un résultat apparaît au bas "
#~ "de la fenere disant :"

#~ msgid "<span class=\"guilabel\">Okay</span>, if the ISO image is correct,"
#~ msgstr "<span class=\"guilabel\">Okay</span>, si l'image ISO est bonne,"

#~ msgid ""
#~ "<span class=\"guilabel\">Match failed!</span>, if the ISO image is not "
#~ "correct."
#~ msgstr ""
#~ "<span class=\"guilabel\">Match failed!</span>, si l'image ISO n'est pas "
#~ "bonne."

#~ msgid ""
#~ "<span class=\"application\">MD5 Reborned Hasher</span> does not work "
#~ "properly with the new download interface introduced in Firefox 20. A "
#~ "workaround is <a href=\"/forum/MD5_Checking_solution_in_new_FireFox"
#~ "\">documented in the forum</a>."
#~ msgstr ""
#~ "<span class=\"application\">MD5 Reborned Hasher</span> ne fonctionne pas "
#~ "correctement avec la nouvelle interface de téléchargement introduite dans "
#~ "Firefox 20. Une solution temporaire est <a href=\"/forum/"
#~ "MD5_Checking_solution_in_new_FireFox\">documentée dans le forum</a>."

#~ msgid ""
#~ "Here is the checksum (a kind of digital fingerprint) of the ISO image for "
#~ "version [[!inline pages=\"inc/stable_i386_version\" raw=\"yes\"]].  "
#~ "Select it with your cursor:"
#~ msgstr ""
#~ "Voici la somme de contrôle (une sorte d'empreinte digitale numérique) de "
#~ "l'image ISO de la version [[!inline pages=\"inc/stable_i386_version\" raw="
#~ "\"yes\"]]. Sélectionnez-la avec votre curseur :"

#~ msgid ""
#~ "Right-click on it and choose \"Selected hash (SHA256)\" from the "
#~ "contextual menu:"
#~ msgstr ""
#~ "Cliquez-droit dessus et choisissez \"Selected hash (SHA256)\" dans le "
#~ "menu contextuel :"

#~ msgid ""
#~ "From the dialog box that shows up, open the ISO image. Then wait for the "
#~ "checksum to compute. This will take several seconds during which your "
#~ "browser will be unresponsive."
#~ msgstr ""
#~ "Dans la fenêtre qui s'ouvre alors, ouvrez l'image ISO. Attendez que la "
#~ "vérification soit effectuée. Cela prendra quelques secondes durant "
#~ "lesquelles votre navigateur sera inutilisable. Soyez patient !"

#~ msgid ""
#~ "<strong>If the ISO image is correct</strong> you will get a notification "
#~ "saying that the checksums match:"
#~ msgstr ""
#~ "<strong>Si la somme de contrôle correspond à l'image ISO </strong>, vous "
#~ "recevrez un message vous l'indiquant :"

#~ msgid ""
#~ "<strong>If the ISO image is not correct</strong> you will get a "
#~ "notification telling you that the checksums do not match:"
#~ msgstr ""
#~ "<strong>Si la somme de contrôle ne correspond pas à l'image ISO </"
#~ "strong>, vous recevrez un message de ce type :"
