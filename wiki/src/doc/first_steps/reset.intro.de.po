# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2013-10-24 01:48+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
msgid ""
"To uninstall Tails from a USB stick or SD card, and use it for something "
"else, you have to reset it."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"**The content of the device will be lost in the operation,** but an attacker\n"
"might still be able to tell that Tails was installed on that device using\n"
"[[data recovery techniques|encryption_and_privacy/secure_deletion]] unless you\n"
"[[securely clean all the available disk space|encryption_and_privacy/secure_deletion#usb_and_ssd]]\n"
"afterwards.\n"
msgstr ""
